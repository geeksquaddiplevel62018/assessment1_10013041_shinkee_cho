<!DOCTYPE html>
<html>

<head>
    <title>Have There</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="./imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./imgs/favicon.ico" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" media="screen" href="./css/common.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./css/template1.css" />
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="./js/jquery-3.3.1.min.js"></script>
    <script src="./js/common.js"></script>
</head>

<body>

<!-- Menu on top -->
<div class="s_top">
    <ul>
        <li><a href="/"><img class="logo" src="./imgs/logo.png"></a></li>
        <li class="companyName"><a href="/">Have There</a></li>
    </ul>

    <ul class="s_menu index">
        <li class="menuTab"><a class="icon">&#9776;</a></li>
        <li class="about"><a href="./about.php">About</a></li>
        <li class="delivery"><a href="./delivery.php">Delivery</a></li>
        <li class="faq"><a href="./faq.php">FAQ</a></li>
        <li class="feedback"><a href="./feedback.php">Feedback</a></li>
        <li class="contactus"><a href="./contactus.php">Contact us</a></li>
        <li class="gap"></li>
        <li class="login"><a href="./login.php">Login</a></li>
        <li class="signup"><a href="./signup.php">Signup</a></li>
    </ul>
</div>


<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <div id='layerA' class='layer'>
            <div class='menuIcon home'></div>
            <div class='menuIcon social facebook'></div>
            <div class='menuIcon social tweeter'></div>
            <div class='menuIcon social youtube'></div>
            <div class='menuIcon social vimeo'></div>
        </div>

        <!-- Recommanded Food -->
        <div id="layerB" class="layer">
            <ul class="foodInfo">
                <li class="name"><h1>Food Name</h1></li>
                <li class="rating"><i class="fa heart rating5"></i> 5.0(300)</li>
                <li class="text">Highly recommended</li>
                <li class="views">
                    <ul>
                        <li class="number">23</li>
                        <li class="text">views</li>
                    </ul>
                    <span class="slash">/</span>
                    <ul>
                        <li class="number">198</li>
                        <li class="text">likes</li>
                    </ul>
                </li>
                <li class="order"><button>Order</button></li>
            </ul>
            <img class="bgFood" src="./imgs/food/petoncle-alfredo.png">
        </div>  

        <!-- Ingredients -->
        <div id="layerC" class="layer">
            <img class="bg" src="./imgs/food/ingredients.png">
            <div class="filter"></div>
            <ul class="Ingredients">
                <li><h1>Ingredients</h1></li>
                <li>
                    <span class="left">200g</span>
                    <span class="right">fresh linguine</span>
                </li>
                <li>
                    <span class="left">2-3</span>
                    <span class="right">cups tomatoes</span>
                </li>
                <li>
                    <span class="left">1/4</span>
                    <span class="right">cup fresh herbs</span>
                </li>
                <li>
                    <span class="left">1-2</span>
                    <span class="right">garlic cloves</span>
                </li>
                <li>
                    <span class="left">4</span>
                    <span class="right">tablespoons oil</span>
                </li>
                <li>
                    <span class="left">1</span>
                    <span class="right">spoon vinegar</span>
                </li>
            </ul>
        </div>

        <!-- Weather ticker -->
        <div id="layerD" class="layer">
            <div id="metservice-widget"></div>
        </div>

        <!-- Testimonial -->
        <div id="layerE" class="layer">
            <img class="bg" src="./imgs/food/fresh_italian_food.png">
            <p>"Good pizza, Mike is awesome."</p>
            <p>- Adam</p>
        </div>

        <!-- Foods -->
        <div id="layerF" class="layer">
            <h1>Foods to delivery</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Add</th>
                </tr>
                
                <tr>
                <td><img src="./imgs/food/pasta.png"></td>
                <td>Pasta</td>
                <td><button class="decrease">-</button> 1 <button class="increase">+</button></td>
                <td>$20.00</td>
                <td><button class="add">Add</button></td>
                </tr>
                
                <tr class="selected">
                <td><img src="./imgs/food/sipa.s-palvelut.png"></td>
                <td>Pizza</td>
                <td><button class="decrease">-</button> 4 <button class="increase">+</button></td>
                <td>$15.00</td>
                <td><button class="add">Add</button></td>
                </tr>
                
                <tr>
                <td><img src="./imgs/food/seafood_salad.png"></td>
                <td>Sea food salad</td>
                <td><button class="decrease">-</button> 2 <button class="increase">+</button></td>
                <td>$18.00</td>
                <td><button class="add">Add</button></td>
                </tr>
                
                <tr>
                <td><img src="./imgs/food/steak.png"></td>
                <td>Grilled steak</td>
                <td><button class="decrease">-</button> 9 <button class="increase">+</button></td>
                <td>$23.00</td>
                <td><button class="add">Add</button></td>
                </tr>
            </table>

            <div class="btns">
                <button class="myCart">Go to MyCart</button>
                <button class="emptyList">Empty List</button>
            </div>
        </div>

        <!-- Selected food -->
        <div id="layerG" class="layer">
            <h1>Pizza Special</h1>
            <p>Price : $15.00</p>
            <div class="ratingBox"><i class="fa heart rating"></i><span>4.8</span></div>
            <img class="bg" src="./imgs/food/sipa.s-palvelut.png">
            <div class="caption">
                <button class="more">MORE</button>
                <span>More information.</span>
            </div>
        </div>
        
        <!-- Twitter feed -->
        <div id="layerH" class="layer">
        <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/search?q=%23italian%20food" data-widget-id="981739317683879936">Tweets about #italian food</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>

        <!-- Bottom -->
        <div id='layerJ' class='layer'>
            <div class='bg'>
                <img src='./imgs/food/bottom.png'>
            </div>

            <div class='gMap'>
                <iframe id='map' width='190' height='160' frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/place?q=Toiohomai%20Bongard%20in%20tauranga&key=AIzaSyDkBxpd8i_6qcACWarvZb6K8VGLrztYGbA&language=en&zoom=14' allowfullscreen></iframe>
            </div>

            <div class='contactus'>
                <h1>Contact us</h1>
                <p><img src='/imgs/phone.png'><span>021 0270 1023</span></p>
                <p><img src='/imgs/email.png'><span>service@havethere.com</span></p>
                <p><img src='/imgs/address.png'><span>162 Love Street, Heaven,</p>
                <p class='newline'><span>Tauranga</span></p>
            </div>
        </div>
    </div>
</div>

<script>
    Layout.arrange("template1");
</script>

<!-- Weather ticker start -->
<script>
(function(d){
var i = d.createElement("iframe");
i.setAttribute("src", "https://services.metservice.com/weather-widget/widget?params=blue|large|portrait|days-3|modern&loc=tauranga&type=urban&domain=" + d.location.hostname);
i.style.width = "300px";
i.style.height = "239px";
i.style.border = "0";
i.setAttribute("allowtransparency", "true");
i.setAttribute("id", "widget-iframe");
d.getElementById("metservice-widget").appendChild(i);
})(document);
</script>
<style>
    #metservice-widget { margin:15px; background: #083156; }
</style>
<!-- Weather ticker end -->

</body>
</html>