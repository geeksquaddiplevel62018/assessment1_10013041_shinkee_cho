<!DOCTYPE html>
<html>

<head>
    <title>Have There</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="./imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./imgs/favicon.ico" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" media="screen" href="./css/common.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./css/template2.css" />
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    
    <script src="./js/jquery-3.3.1.min.js"></script>
    <script src="./js/common.js"></script>
</head>

<body>

<!-- Menu on top -->
<div class="s_top">
    <ul>
        <li><a href="/"><img class="logo" src="./imgs/logo.png"></a></li>
        <li class="companyName"><a href="/">Have There</a><li>
    </ul>

    <ul class="s_menu about">
        <li class="menuTab"><a href="#" class="icon">&#9776;</a></li>
        <li class="about"><a href="./about.php">About</a></li>
        <li class="delivery"><a href="./delivery.php">Delivery</a><li>
        <li class="faq"><a href="./faq.php">FAQ</a><li>
        <li class="feedback"><a href="./feedback.php">Feedback</a><li>
        <li class="contactus"><a href="./contactus.php">Contact us</a><li>
        <li class="gap"></li>   
        <li class="login"><a href="./login.php">Login</a><li>
        <li class="signup"><a href="./signup.php">Signup</a><li>
    </ul>
</div>

<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <div id='layerA' class='layer'>
            <div class='menuIcon home'></div>
            <div class='menuIcon social facebook'></div>
            <div class='menuIcon social tweeter'></div>
            <div class='menuIcon social youtube'></div>
            <div class='menuIcon social vimeo'></div>
        </div>

        <!-- Slogan -->
        <div id="layerB" class="layer">
            <span class="slogan"><q class="q1"></q>Have <b>here</b>? No! Have <b>There!</b><q class="q3"></q></span>
        </div>
        
        <!-- About us -->
        <div id="layerD" class="layer">
            <h1>About us</h1>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna
            aliqua. Ut enim ad minim veniam, quis nostrud exercitation
            ullamco laboris nisi ut. 
            </p>

            <img src="./imgs/ceo.png">

            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>

            <p class="ceo">
            CEO Micelle
            </p>
        </div>
        
        <!-- Chef 1 -->
        <div id="layerE" class="layer">
            <p><q class="q1"></q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <p>Tanya Blazelist,<br>
               Master Chef</p>
            <img src="./imgs/chef1.png">
        </div>

        <!-- Chef 2 -->
        <div id="layerF" class="layer">
            <p><q class="q1"></q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <p>Laura Mercier,<br>
               Master Chef</p>
            <img src="./imgs/chef2.png">
        </div>

        <!-- Grand Open -->
        <div id="layerH" class="layer">
            <img src="./imgs/grand_open.png">
        </div>

        <!-- Staff photo -->
        <div id="layerI" class="layer">
            <img src="./imgs/staff.jpg">
        </div>

        <!-- Bottom -->
        <div id='layerJ' class='layer'>
            <div class='bg'>
                <img src='./imgs/food/bottom.png'>
            </div>

            <div class='gMap'>
                <iframe id='map' width='190' height='160' frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/place?q=Toiohomai%20Bongard%20in%20tauranga&key=AIzaSyDkBxpd8i_6qcACWarvZb6K8VGLrztYGbA&language=en&zoom=14' allowfullscreen></iframe>
            </div>

            <div class='contactus'>
                <h1>Contact us</h1>
                <p><img src='/imgs/phone.png'><span>021 0270 1023</span></p>
                <p><img src='/imgs/email.png'><span>service@havethere.com</span></p>
                <p><img src='/imgs/address.png'><span>162 Love Street, Heaven,</p>
                <p class='newline'><span>Tauranga</span></p>
            </div>
        </div>
    </div>
</div>

<script>
    Layout.arrange("template2");
</script>

</body>
</html>