<!DOCTYPE html>
<html>

<!-- Head tag -->
<head>
    <title>Have There</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="./imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./imgs/favicon.ico" type="image/x-icon">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/common.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./css/template4.css" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="./js/jquery-3.3.1.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/common.js"></script>
</head>

<body>

<!-- Menu on top -->
<div class="s_top">
    <ul>
        <li><a href="/"><img class="logo" src="./imgs/logo.png"></a></li>
        <li class="companyName"><a href="/">Have There</a><li>
    </ul>

    <ul class="s_menu faq">
        <li class="menuTab"><a href="#" class="icon">&#9776;</a></li>
        <li class="about"><a href="./about.php">About</a></li>
        <li class="delivery"><a href="./delivery.php">Delivery</a><li>
        <li class="faq"><a href="./faq.php">FAQ</a><li>
        <li class="feedback"><a href="./feedback.php">Feedback</a><li>
        <li class="contactus"><a href="./contactus.php">Contact us</a><li>
        <li class="gap"></li> 
        <li class="login"><a href="./login.php">Login</a><li>
        <li class="signup"><a href="./signup.php">Signup</a><li>
    </ul>
</div>

<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <div id='layerA' class='layer'>
            <div class='menuIcon home'></div>
            <div class='menuIcon social facebook'></div>
            <div class='menuIcon social tweeter'></div>
            <div class='menuIcon social youtube'></div>
            <div class='menuIcon social vimeo'></div>
        </div>

        <!-- Recommendation -->
        <div id="layerB" class="layer">
            <ul class="foodInfo">
                <li class="name"><h1>Food Name</h1></li>
                <li class="rating"><i class="fa heart rating5"></i> 5.0(210)</li>
                <li class="text">Highly recommended</li>
                <li class="views">
                    <ul>
                        <li class="number">40</li>
                        <li class="text">views</li>
                    </ul>
                    <span class="slash">/</span>
                    <ul>
                        <li class="number">120</li>
                        <li class="text">likes</li>
                    </ul>
                </li>
                <li class="order"><button>Order</button></li>
            </ul>
            <img class="bgFood" src="./imgs/food/sipa.s-palvelut.png">
        </div> 


        <!-- FAQ Form -->
        <div id="layerD" class="layer">
            <div class="faq">
                <div class="title">FAQ</div>  
                <div class="listBox">
                    <ul class="list">
                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer1">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer1" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer2">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer2" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer3">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer3" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer4">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer4" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer5">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer5" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer6">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer6" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer7">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer7" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer8">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer8" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer9">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer9" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer10">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer10" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer11">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer11" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer12">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit?
                                </button>
                                <div id="answer12" class="answer collapse in">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <!-- Testimonial -->
        <div id="layerE" class="layer">
            <img class="bg" src="./imgs/food/fresh_italian_food.png">
            <p>"Good pizza, Mike is awesome."</p>
            <p>- Adam</p>
        </div>
        
        <!-- Favorite Foods -->
        <div id="layerH" class="layer">
            <h1>Favorite Foods</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Price</th>
                <th>Rating</th>
                <th>Order</th>
                </tr>
                
                <tr>
                <td><img src="./imgs/food/pasta.png"></td>
                <td>Pasta</td>
                <td>$20.00</td>
                <td><i class="fa heart rating5"></i><br>5.0(190)</td>
                <td>233</td>
                </tr>
                
                <tr class="">
                <td><img src="./imgs/food/sipa.s-palvelut.png"></td>
                <td>Pizza</td>
                <td>$15.00</td>
                <td><i class="fa heart rating5"></i><br>5.0(162)</td>
                <td>201</td>
                </tr>
                
                <tr>
                <td><img src="./imgs/food/seafood_salad.png"></td>
                <td>Sea food salad</td>
                <td>$18.00</td>
                <td><i class="fa heart rating5"></i><br>5.0(103)</td>
                <td>172</td>
                </tr>
                
                <tr>
                <td><img src="./imgs/food/steak.png"></td>
                <td>Grilled steak</td>
                <td>$23.00</td>
                <td><i class="fa heart rating4"></i><br>4.0(50)</td>
                <td>98</td>
                </tr>

                <tr>
                <td><img src="./imgs/food/grilled_panini.png"></td>
                <td>Grilled panini</td>
                <td>$10.00</td>
                <td><i class="fa heart rating4"></i><br>4.0(36)</td>
                <td>87</td>
                </tr>
            </table>
        </div>
        
        <!-- Bottom -->
        <div id='layerJ' class='layer'>
            <div class='bg'>
                <img src='./imgs/food/bottom.png'>
            </div>

            <div class='gMap'>
                <iframe id='map' width='190' height='160' frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/place?q=Toiohomai%20Bongard%20in%20tauranga&key=AIzaSyDkBxpd8i_6qcACWarvZb6K8VGLrztYGbA&language=en&zoom=14' allowfullscreen></iframe>
            </div>

            <div class='contactus'>
                <h1>Contact us</h1>
                <p><img src='/imgs/phone.png'><span>021 0270 1023</span></p>
                <p><img src='/imgs/email.png'><span>service@havethere.com</span></p>
                <p><img src='/imgs/address.png'><span>162 Love Street, Heaven,</p>
                <p class='newline'><span>Tauranga</span></p>
            </div>
        </div>
    </div>
</div>

<script>
    Layout.arrange("template4");
</script>

</body>
</html>