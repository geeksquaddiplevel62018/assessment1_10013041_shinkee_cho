USE `container-db`;

DROP TABLE IF EXISTS feedback;
DROP TABLE IF EXISTS contactus;
DROP TABLE IF EXISTS faq;
DROP TABLE IF EXISTS food;
DROP TABLE IF EXISTS testimonial;
DROP TABLE IF EXISTS member;

CREATE TABLE member (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    EMAIL VARCHAR(255) NOT NULL,
    FNAME VARCHAR(255) NOT NULL,
    LNAME VARCHAR(255),
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE testimonial (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(255) NOT NULL,
    MESSAGE TEXT NOT NULL,
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE food (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(255) NOT NULL,
    RATING_LEVEL FLOAT(2,1) UNSIGNED NOT NULL,
    RATING_CNT INT UNSIGNED NOT NULL,
    VIEWS INT UNSIGNED NOT NULL,
    ORDERS INT UNSIGNED NOT NULL,
    PRICE FLOAT(5,2) UNSIGNED NOT NULL,
    F_DESC TEXT NOT NULL,
    IMG_PATH VARCHAR(255) NOT NULL,
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE faq (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    QUESTION VARCHAR(255) NOT NULL,
    ANSWER TEXT NOT NULL,
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE contactus (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(255) NOT NULL,
    EMAIL VARCHAR(255) NOT NULL,
    PHONE VARCHAR(15) NOT NULL,
    PURPOSE VARCHAR(20) NOT NULL,
    MESSAGE TEXT NOT NULL,
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE feedback (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    SATISFACTION TINYINT(1) UNSIGNED,
    IMPROVEMENT TEXT,
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

-- *********************************
--          CREATE MEMBER 
-- *********************************

INSERT INTO member (EMAIL,FNAME,LNAME) VALUES ('jeff@abc.com', 'Jeff', 'Kranenburg');
INSERT INTO member (EMAIL,FNAME,LNAME) VALUES ('steve@abc.com', 'Steve', 'Stevenson');
INSERT INTO member (EMAIL,FNAME,LNAME) VALUES ('roger@abc.com', 'Roger', 'Rogers');
INSERT INTO member (EMAIL,FNAME,LNAME) VALUES ('kevin@abc.com', 'Kevin', 'Bateman');
INSERT INTO member (EMAIL,FNAME,LNAME) VALUES ('sing@abc.com', 'Sing', 'Cho');
INSERT INTO member (EMAIL,FNAME,LNAME) VALUES ('vinncent@abc.com', 'Vinccent', 'Ning');

SELECT * FROM member;

-- *********************************
--       CREATE TESTIMONIAL 
-- *********************************

INSERT INTO testimonial (NAME,MESSAGE) VALUES ('Jeff Kranenburg', 'I am very pleased with the service and the quality of food is very good. I recommend this place to all my friends.');
INSERT INTO testimonial (NAME,MESSAGE) VALUES ('Alisha H.', 'I have visited too many Indian restaurants and this is my favorite by far. The food is amazing and worth the drive.');
INSERT INTO testimonial (NAME,MESSAGE) VALUES ('Marcel N.', 'Very tasty food, reasonable prices. Atmosphere is special, but the food is great. Try the chicken tikka masala...');
INSERT INTO testimonial (NAME,MESSAGE) VALUES ('Hotfood1', 'Excellent lunch buffet. really authentic and tasty when compared to other restaurants in MN.. I would go again. Pros: spicy and authentic food');
INSERT INTO testimonial (NAME,MESSAGE) VALUES ('Vidya', "I love their buffet's, go early, and its fresh and hot, with lots of choices!!!! yum!!");
INSERT INTO testimonial (NAME,MESSAGE) VALUES ('Lalit Meesala', 'Best Indian Restaurant in Twin Cities..... Good Service, Lot of varities...');

SELECT * FROM testimonial;

-- *********************************
--           CREATE FOOD
-- *********************************

INSERT INTO food (NAME,RATING_LEVEL,RATING_CNT,VIEWS,ORDERS,PRICE,F_DESC,IMG_PATH) VALUES ('Tubular Pasta', 4.2, 123, 200, 80, 20.00, 'Tubular pastas are any pastas that are in the shape of a tube. They are available in many different sizes and shapes. Some tubes are long and narrow while others are short and wide. They are found with smooth or grooved exteriors. They are often served with a heavy sauce, but are also used in salads and casseroles.', '/imgs/food/pasta.png');
INSERT INTO food (NAME,RATING_LEVEL,RATING_CNT,VIEWS,ORDERS,PRICE,F_DESC,IMG_PATH) VALUES ('Petoncle Alfredo', 5.0, 63, 100, 143, 15.00, 'Sliced grilled chicken and our signature, homemade alfredo sauce over fettuccine pasta.', '/imgs/food/petoncle-alfredo.png');
INSERT INTO food (NAME,RATING_LEVEL,RATING_CNT,VIEWS,ORDERS,PRICE,F_DESC,IMG_PATH) VALUES ('Chicken Salad', 3.5, 200, 454, 343, 15.00, 'Fried chicken tenders, fresh Asian greens, rice noodles, almonds with an Oriental vinaigrette.', '/imgs/food/chicken_salad.png');
INSERT INTO food (NAME,RATING_LEVEL,RATING_CNT,VIEWS,ORDERS,PRICE,F_DESC,IMG_PATH) VALUES ('Grilled Panini', 5.0, 235, 253, 214, 12.00, 'Grilled Vegetable Panini loaded with zucchini, bell peppers and red onion. Topped with mozzarella and an Italian herb olive oil spread. Quick vegetarian meal!', '/imgs/food/grilled_panini.png');
INSERT INTO food (NAME,RATING_LEVEL,RATING_CNT,VIEWS,ORDERS,PRICE,F_DESC,IMG_PATH) VALUES ('Seafood Salad', 4.8, 35, 89, 50, 23.00, 'Toss roasted vegetables and salmon with a flavor-packed vinaigrette to serve on top of greens for a hearty dinner salad. For a twist, add a poached or fried egg on top. Serve with: Toasted whole-grain baguette and a glass of Riesling.', '/imgs/food/pasta.png');
INSERT INTO food (NAME,RATING_LEVEL,RATING_CNT,VIEWS,ORDERS,PRICE,F_DESC,IMG_PATH) VALUES ('Beef Steak', 4.1, 600, 1002, 876, 32.00, "Tender flat iron steak meets trendy Asian bistro flavor! With a side of Asian vegetable mix, it's an easy delicious dinner!", '/imgs/food/steak.png');

SELECT * FROM food;

-- *********************************
--       CREATE FAQ 
-- *********************************

INSERT INTO faq (QUESTION,ANSWER) VALUES ("I’ve just placed an order, when will I get my delivery?", "We charge one week in advance. Payments are taken on Sunday for delivery the following Sunday. For example, if you order on Wednesday, your first payment will be processed on the Sunday with your first delivery arriving on the following Sunday.");
INSERT INTO faq (QUESTION,ANSWER) VALUES ("I’ve missed the Sunday cut off, can I still get a box this week?", "All of our orders are placed with our suppliers on Monday, and then the hard work of getting the boxes ready for delivery starts. However, it’s worth sending us an email to see if we can squeeze you in.");
INSERT INTO faq (QUESTION,ANSWER) VALUES ("When do I get charged?","We charge one week in advance, payments are taken on Sunday for delivery the following Sunday.");
INSERT INTO faq (QUESTION,ANSWER) VALUES ("What time does my Green Dinner Table box arrive?", "All deliveries are between 3 pm and 6:30 pm on Sunday. If you haven’t received your box by 6:30 pm please get in touch with us ASAP.");
INSERT INTO faq (QUESTION,ANSWER) VALUES ("Something is missing from my box?", "All Green Dinner Table boxes are packed lovingly by hand, and while rare, sometimes we might miss an item from a box. If this happened, please get in contact ASAP.");
INSERT INTO faq (QUESTION,ANSWER) VALUES ("Can you do gluten-free boxes?","Yes! Please email us at info@greendinnertable.co.nz for more info. We are not a gluten-free kitchen, but we will replace items to accommodate for the needs of our gluten-free customers.");

SELECT * FROM faq;

-- *********************************
--       CREATE CONTACTUS 
-- *********************************

INSERT INTO contactus (NAME,EMAIL,PHONE,PURPOSE,MESSAGE) VALUES ('Jeff Kranenburg', 'jeff@abc.com', '0210343043', 'claim', 'I think I got a wrong receipt. Can you refund it?');
INSERT INTO contactus (NAME,EMAIL,PHONE,PURPOSE,MESSAGE) VALUES ('Steve Stevenson', 'steve@abc.com', '073948374', 'query', 'Can you deliver to rural area?');
INSERT INTO contactus (NAME,EMAIL,PHONE,PURPOSE,MESSAGE) VALUES ('Roger .K', 'roger@abc.com', '02984755758', 'query', 'How long does it take?');
INSERT INTO contactus (NAME,EMAIL,PHONE,PURPOSE,MESSAGE) VALUES ('Kevin Bateman', 'kevin@abc.com', '02983757384', 'other', "Are you free? I'd like to meet and have dinner with you tonight. Can I?");
INSERT INTO contactus (NAME,EMAIL,PHONE,PURPOSE,MESSAGE) VALUES ('Sing Cho', 'sing@abc.com', '02185628487', 'claim', "Your service is awful. Food is terrible!!");
INSERT INTO contactus (NAME,EMAIL,PHONE,PURPOSE,MESSAGE) VALUES ('Vinccent', 'vinncent@abc.com', '0218473939', 'query', 'What time do you close?');

SELECT * FROM contactus;

-- *********************************
--       CREATE FEEDBACK 
-- *********************************

INSERT INTO feedback (SATISFACTION,IMPROVEMENT) VALUES (5, NULL);
INSERT INTO feedback (SATISFACTION,IMPROVEMENT) VALUES (4, 'Please, give him smile when you face a customer.');
INSERT INTO feedback (SATISFACTION,IMPROVEMENT) VALUES (1, 'There is no way to improve your service.');
INSERT INTO feedback (SATISFACTION,IMPROVEMENT) VALUES (4, NULL);
INSERT INTO feedback (SATISFACTION,IMPROVEMENT) VALUES (4, NULL);
INSERT INTO feedback (SATISFACTION,IMPROVEMENT) VALUES (5, NULL);

SELECT * FROM feedback;